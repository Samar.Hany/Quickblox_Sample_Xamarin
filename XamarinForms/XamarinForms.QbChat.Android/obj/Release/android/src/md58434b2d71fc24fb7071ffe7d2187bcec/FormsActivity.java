package md58434b2d71fc24fb7071ffe7d2187bcec;


public class FormsActivity
	extends md5b60ffeb829f638581ab2bb9b1a7f4f3f.FormsApplicationActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("XamarinForms.QbChat.Android.FormsActivity, XamarinForms.Qmunicate.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", FormsActivity.class, __md_methods);
	}


	public FormsActivity () throws java.lang.Throwable
	{
		super ();
		if (getClass () == FormsActivity.class)
			mono.android.TypeManager.Activate ("XamarinForms.QbChat.Android.FormsActivity, XamarinForms.Qmunicate.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
